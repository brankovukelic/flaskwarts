import unittest
import os

import mock
import flask
import formencode

with mock.patch('flask.g') as g:

    from utils.forms import Form


    class MyForm(Form):
        template_name = 'test_template.html'

        def render_base(self):
            return """
            <form action="/" method="POST">
                <p>
                <input name="foo" type="email" form:validate="email" form:required="true">
                <form:error name="foo">
                </p>
                <input type="hidden" name="bar" value="12">
                <input type="submit" value="Submit">
            </form>
            """


    class MyCustomValidationForm(Form):
        validators = {'foo': True}

        def render_base(self):
            return 'foo'


    class FormTestCase(unittest.TestCase):

        def test_parsing_schema(self):
            form = MyForm()
            schema = form.get_schema()
            self.assertIn('foo', schema.fields)
            self.assertTrue(isinstance(schema.fields['foo'],
                                       formencode.compound.All))
            validators = schema.fields['foo'].validators
            self.assertTrue(isinstance(validators[1],
                                       formencode.validators.NotEmpty))
            self.assertTrue(isinstance(validators[2],
                                       formencode.validators.Email))

        def test_filling_form(self):
            form = MyForm()
            s = form.render()
            self.assertNotIn('<form:error name="foo">', s)

        def test_filling_preserves_existing(self):
            """ should not strip data when defaults don't exit for them """
            form = MyForm()
            s = form.render()
            self.assertIn('<input type="hidden" name="bar" value="12">', s)

        def test_filling_form_with_data(self):
            form = MyForm({'foo': 'foo@test.com'})
            s = form.render()
            self.assertIn('value="foo@test.com"', s)

        def test_validation(self):
            form = MyForm({'foo': 'abc'})
            self.assertFalse(form.is_valid)
            self.assertEqual(form.valid_data, {})

            form = MyForm({'foo': 'foo@test.com'})
            self.assertTrue(form.is_valid)
            self.assertEqual(form.valid_data['foo'], 'foo@test.com')

        def test_render_invalid_form(self):
            form = MyForm({'foo': None})
            self.assertFalse(form.is_valid)
            s = form.render()
            self.assertIn('<span class="error-message">Please enter a value'
                          '</span>', s)

        def test_csrf_token_setting(self):
            """ stale token should be refreshed from the flask.g variable """
            g.csrf_token = 'bar'
            form = MyForm({'_csrf_token': 'foo'})
            s = form.render()
            self.assertEqual(form.data['_csrf_token'], 'bar')

        @mock.patch('utils.forms.parse_schema')
        def test_passing_custom_validators(self, parse):
            f = MyCustomValidationForm()
            f.get_schema()
            parse.assert_called_once_with('foo', f.validators)

        @mock.patch('utils.schemabuilder.ArgumentCleaningSchemaBuilder')
        def test_custom_validators_passed_to_schema(self, Builder):
            f = MyCustomValidationForm()
            f.get_schema()
            Builder.assert_called_once_with(f.validators)

    if __name__ == '__main__':
        unittest.main()
