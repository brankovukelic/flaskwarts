import unittest

import mock
from flask import request, make_response

from utils import crypto


class CsrfTestCase(unittest.TestCase):

    @mock.patch('os.urandom')
    @mock.patch('hashlib.sha512')
    def test_random_string_generator(self, sha, urandom):
        """ should return random sha hexdigest urandom """
        urandom.return_value = 'foobar'
        crypto.random_hash(12)
        urandom.assert_called_once_with(12)
        sha.return_value.update.assert_called_once_with(urandom.return_value)
        sha.return_value.hexdigest.assert_called_once()

    def test_csrf_token_sanitizing(self):
        t = crypto.random_hash(crypto.TOKEN_BYTES)
        self.assertEqual(crypto.clean_csrf_token(t), t)
        self.assertNotEqual(crypto.clean_csrf_token('bogus'), 'bogus')

    def test_same_origin(self):
        """ should catch urls that are not same-origin """
        url1 = 'http://example.com'
        url2 = 'http://www.example.com/foo'
        url3 = 'http://www.example.com/foobar'
        url4 = 'https://www.example.com/foo'

        self.assertFalse(crypto.same_origin(url1, url2))
        self.assertFalse(crypto.same_origin(url1, url3))
        self.assertTrue(crypto.same_origin(url2, url3))
        self.assertFalse(crypto.same_origin(url3, url4))


class CsrfCheckTestCase(unittest.TestCase):

    def get_request(self, method='POST', referer='http://example.com/',
                    url='http://example.com/', token=None):
        request = mock.MagicMock()
        request.url = url
        request.method = method
        if referer:
            request.headers = {'Referer': referer}
            request.environ = {'HTTP_REFERER': referer}
        else:
            request.headers = dict()
        request.cookies = {'_csrf_token': token}
        return request


    def test_safe_method(self):
        """ shouldn't care about GET, HEAD, OPTION, and TRACE methods """
        for method in ['GET', 'HEAD', 'OPTION', 'TRACE']:
            r = self.get_request(method=method)
            try:
                crypto.check_csrf_token(r, 'foo')
            except Exception:
                self.assertFalse(True, 'Should not raise on %s' % method)

    def test_missing_referer(self):
        """ should raise on missing HTTP_REFERER """
        r = self.get_request(referer=None)
        with self.assertRaises(crypto.CsrfError) as ce:
            crypto.check_csrf_token(r, 'foo')
            self.assertEqual(ce.reason, crypto.CsrfError.NO_REFERER)

    def test_wrong_referer(self):
        """ should raise on referer mismatch """
        r = self.get_request(referer='http://example.com/',
                             url='http://www.example.com/')
        with self.assertRaises(crypto.CsrfError) as ce:
            crypto.check_csrf_token(r, 'foo')
            self.assertEqual(ce.reason, crypto.CsrfError.WRONG_REFERER)

    def test_missing_token(self):
        """ should raise if token is not in the cookies """
        r = self.get_request()
        with self.assertRaises(crypto.CsrfError) as ce:
            crypto.check_csrf_token(r, 'foo')
            self.assertEqual(ce.reason, crypto.CsrfError.MISSING)

    def test_wrong_token(self):
        """ should raise if tokens are not equal """
        r = self.get_request(token='foo')
        with self.assertRaises(crypto.CsrfError) as ce:
            crypto.check_csrf_token(r, 'bar')
            self.assertEqual(ce.reason, crypto.CsrfError.MISMATCH)

    @mock.patch('utils.crypto.clean_csrf_token')
    def test_correct_token(self, clean):
        r = self.get_request(token='foo')
        try:
            crypto.check_csrf_token(r, 'foo')
        except Exception, err:
            self.assertFalse(True, 'Should not raise %s if tokens match' % err)


if __name__ == '__main__':
    unittest.main()
