import unittest
import Cookie

import mock
import flask
from werkzeug.test import EnvironBuilder
from werkzeug.wrappers import Response

from utils.middlewares import csrf
from utils import crypto


def ce(*args, **kwargs):
    """ Raise generic CSRF exception """
    raise crypto.CsrfError('Failed', reason=0)


class CsrfMiddlewareTestCase(unittest.TestCase):

    def get_app(self, response=None, environ=None):
        app = mock.MagicMock()
        app.config = {
            'CSRF_TOKEN_NAME': 'CSRF_TOKEN',
            'CSRF_ARG_NAME': '_csrf_token',
        }
        app.return_value = response or Response('foo', status=200)
        return app

    def get_env(self, *args, **kwargs):
        return EnvironBuilder(*args, **kwargs).get_environ()

    def create_app(self):
        app = flask.Flask(__name__)
        app.config.update({
            'DEBUG': False,
            'TESTING': True,
            'SECRET_KEY': 'secret my foo!',
            'CSRF_ARG_NAME': '_csrf_token',
            'CSRF_TOKEN_MAXAGE': 24 * 60 * 60  # 12 hours
        })
        csrf(app)
        app.config['SERVER_NAME'] = 'foo.com'

        @app.route('/csrf', methods=['GET', 'POST'])
        def view_func(*args, **kwargs):
            return 'foo'

        return app

    @mock.patch('utils.crypto.random_hash')
    def test_setting_cookie_in_response(self, rhash):
        """ integration test for CSRF middleware """
        rhash.return_value = 'foo'
        app = self.create_app()
        with app.test_client(use_cookies=True) as c:
            r = c.get('/csrf')
            self.assertIn('foo', dict(r.headers)['Set-Cookie'])

    def test_check_request_status_with_correct_token(self):
        token = crypto.random_hash()
        app = self.create_app()
        with app.test_client(use_cookies=True) as c:
            c.set_cookie('foo.com', '_csrf_token=%s' % token)
            r = c.post('/csrf',
                       base_url='http://foo.com',
                       headers=[('Referer', 'http://foo.com/')],
                       data={'_csrf_token': token})
            self.assertEqual(r.status, '200 OK')

    def test_missing_cookie(self):
        token = crypto.random_hash()
        app = self.create_app()
        with app.test_client(use_cookies=True) as c:
            r = c.post('/csrf',
                       base_url='http://foo.com',
                       headers=[('HTTP_REFERER', 'http://foo.com/')],
                       data={'_csrf_token': token})
            self.assertEqual(r.status, '403 FORBIDDEN')

    def test_missing_form_data(self):
        token = crypto.random_hash()
        app = self.create_app()
        with app.test_client(use_cookies=True) as c:
            c.set_cookie('foo.com', '_csrf_token=%s' % token)
            r = c.post('/csrf',
                       base_url='http://foo.com',
                       headers=[('HTTP_REFERER', 'http://foo.com/')])
            self.assertEqual(r.status, '403 FORBIDDEN')


if __name__ == '__main__':
    unittest.main()
