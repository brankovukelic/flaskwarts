import unittest

import utils
from utils.middlewares import csrf


class ImportObjectTestCase(unittest.TestCase):

    def test_import(self):
        """ should be able to import stuff using simple string """
        t = utils.import_object('utils.middlewares.csrf')
        self.assertEqual(t, csrf)

    def test_import_error(self):
        """ should raise on missing object """
        with self.assertRaises(ImportError):
            utils.import_object('utils.middlewares.bogus')

    def test_import_missing_module(self):
        """ should raise on missing mogule """
        with self.assertRaises(ImportError):
            utils.import_object('foo.bar.baz')


class DecamelizeTestCase(unittest.TestCase):

    def test_basic_decamelizeation(self):
        """ should decamelize camelCase strings """
        s = utils.decamelize('CamelCase')
        self.assertEqual(s, 'camel_case')

    def test_with_first_character_lower_cased(self):
        """ should work even if first character is lower-case """
        s = utils.decamelize('camelCase')
        self.assertEqual(s, 'camel_case')

    def test_works_with_number(self):
        """ should allow numbers """
        s = utils.decamelize('Camel4Case')
        self.assertEqual(s, 'camel4_case')

    def tgest_works_with_underscore(self):
        """ should strip any underscores """
        s = utils.decamelize('Camel_case_not_really')
        self.assertEuqal(s, 'camelcasenotreally')


if __name__ == '__main__':
    unittest.main()
