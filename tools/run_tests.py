#!/usr/bin/env python

""" Test runner for Flask applications """

import unittest
import sys
from flask import Flask


def main():
    """ Run tests """
    # Set up a dummy application context
    # http://flask.pocoo.org/docs/appcontext/
    app = Flask('utils')
    app.config.update({
        'DEBUG': False,
        'TESTING': True,
        'SECRET_KEY': 'secret my foo!',
        'SESSION_COOKIE_NAME': 'session',
    })

    with app.app_context():
        suite = unittest.loader.TestLoader().discover('tests')
        unittest.TextTestRunner(verbosity=1).run(suite)


if __name__ == '__main__':
    sys.path.insert(0, '.')
    main()

